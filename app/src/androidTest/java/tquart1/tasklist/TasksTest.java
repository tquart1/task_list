package tquart1.tasklist;


import android.support.test.filters.SmallTest;
import android.test.ActivityInstrumentationTestCase;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by user on 2/27/2017.
 */

public class TasksTest extends ActivityInstrumentationTestCase<MainActivity> {



    public TasksTest(String pkg, Class<MainActivity> activityClass) {
        super(pkg, activityClass);
    }

    @SmallTest
    public void editText(){
        TextView tv = (TextView) getActivity().findViewById(R.id.editTaskDescription);
        assertNull(tv);
    }

    public void testButtonP1(){
        Button P1 = (Button)getActivity().findViewById(R.id.buttonP1);
        assertNotNull(P1);
    }
    public void testButtonP2(){
        Button P2 = (Button)getActivity().findViewById(R.id.buttonP2);
        assertNotNull(P2);
    }



}
